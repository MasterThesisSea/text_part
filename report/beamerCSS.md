# Fonts

By default, `beamer` uses `sans serif` font, so `\setmainfont` will do nothing. You have to tell it that you want serif fonts:
[...](https://tex.stackexchange.com/a/79423/127717)

Fonts list or Fonts list sheet in beamer.

The LaTeX Font Catalogue
[...](http://www.tug.dk/FontCatalogue/allfonts.html)

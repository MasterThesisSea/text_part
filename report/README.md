Tests of the gravitational inverse-square

Why we do it?
* While it is conventionally assumed that the ISL should be valid for separations from infinity to roughly the Planck length.It had, until a few years ago, only been precisely tested for separations ranging from the scale of the solar system down to a few millimeters. The reasons for this are obvious: on the one hand there are no independently known mass distributions on length scales larger than the solar system, and on the other hand, it is difficult to get enough matter in close enough proximity to obtain a background free gravitational signal at length scales smaller than 1 mm. This contrasts strongly
with Coulomb’s Law (and its electroweak generalization) which has been tested
for separations down to 10^(−18) leptonic interactions at high-energy
colliders(3). Although Coulomb’s Law has not been experimentally verified at
length scales large compared to laboratory dimensions, a null-type laboratory
measurement looking for effects of the galactic electromagnetic vector potential, A, rules out deviations due a finite photon mass for length scales up to ∼2×10^10m

[@](http://www.physics.umd.edu/courses/Phys798g/Paik07/ISL_review.pdf)
* If indeed gravity becomes strong at TeV energies,
gravitons should be radiated at significant rates in high energy particle collisions.

Radiation leads to missing-energy signatures in which a photon or a jet is produced with no observable particle balancing its transverse momentum.
* The relevant processes for LHC are gg → gG, qg → qG and qq → Gg which give rise to final states of jets plus missing transverse energy ( E T ) and qq → Gγ which gives rise to final states with a photon plus  E T . Final states of Z +  E T are not considered as the effective rates are much lower since the Z can only be observed at LHC via its leptonic decay.

To compute the rate of emission of a single G particle, we interpret the G momentum in the extra dimensions as a four-dimensional mass for this spin-2 particle and use the Lagrangian


Hierarchy Problem
* [A technical explanation](https://physics.stackexchange.com/a/43596/146416)
* [A much more handy explanation](https://profmattstrassler.com/articles-and-posts/particle-physics-basics/the-hierarchy-problem/)
  The mass of the smallest possible black hole defines what is known as the Planck Mass.The masses of the W and Z particles, the force carriers of the weak nuclear force, are about 10,000,000,000,000,000 times smaller than the Planck Mass. Thus there is a huge hierarchy in the mass scales of weak nuclear forces and gravity.

Higgs field average value is non-zero

Why does the Higgs field have a Mexican hat shape?
[...](https://www.quora.com/Why-does-the-Higgs-field-have-a-Mexican-hat-shape

Rely on an excellent $\LaTeX$ template With
* Clear structure
* Light-weight design
* Various of character

Continuous integration and Continuous deployment for Latex's projects
* [Continuous deployment for LaTeX's projects with Gitlab CI](https://codingwithcoffee.com.ve/2017/10/01/latex-ci-tutorial.html)
* [Setting up GitLab to automatically generate PDFs from committed LaTeX files](https://sayantangkhan.github.io/latex-gitlab-ci.html)

pdflatex document.tex
pdflatex report/tex

![](https://journals.aps.org/prl/article/10.1103/PhysRevLett.116.131101/figures/5/medium)

避免

* 在看文章时只对原话做了标记，这种糊囵吞枣，一味求快的方式让我停滞。
* 闲暇之余，用科普书培养的“物理直觉” 夸夸其谈，而不是直接围绕问题学习。
* 没有及时总结成自己的话，回头来看，书还是那本书，我还是没长进多少。
* 我不生产思想，只是思想的搬运工。
* 我会想着懂的人看到我的讲述会笑话我，实际上不用这样，因为你也不觉得他们会高明到哪里去，找文章
  参照，模仿他们的思路。模仿，模仿。
* 严格遵照计划
这一部分的主要风格应该是文献综述类型的，应通过文献综述达到让读者相信，本论文的研究领域上虽然已经
有大量成果出现，但前人的研究仍然亟须改进或着推广。


To know the exact scatter of two quarks is not possible, but the transverse
components are not affected by the Lorenz transformation that would take the quark projectiles to their center of mass system.

For the ADD model, two parameterizations for virtual graviton exchange are considered,
Giudice–Rattazzi–Wells (GRW)  and Han–Lykken–Zhang (HLZ) . In the GRW convention, the
sum over the Kaluza–Klein graviton excitations in the effective field theory is regulated by a
single cutoff parameter Λ T . In the HLZ convention, the effective theory is described in terms
of two parameters, the cutoff scale M S and the number of extra spatial dimensions n ED . The
parameters M S and n ED are directly related to Λ T [80]. We consider models with 2–6 EDs. The
case of n ED = 1 is not considered since it would require an ED of the size of the order of the
solar system; the gravitational potential at these distances would be noticeably modified and
this case is therefore excluded by observation. The case of n ED = 2 is special in the sense that
√
the relation between M S and Λ T also depends on the parton-parton center-of-mass energy ŝ.
The ADD predictions are calculated with PYTHIA 8.

Lastly, the lower limits are also computed on the fundamental scale M D in the context of the
large extra dimensional model where the exclusion is found to be varying between 10 TeV for
the number of extra dimensions n = 2 to 5.5 TeV for n = 6.

#### Effective field theory
[Review by Howard Geogi](http://www.people.fas.harvard.edu/~hgeorgi/)

Effective field theory is a standard technique:
- calculate quantum effects at a given energy scale
- shifts focus from U.V. to I.R.
- handles main obstacle
   - quantum effects involve all scales


EFTs are particularly well suited to problems with multiple physical scales
* Effective field theory (EFT) for non-relativistic gravitational systems.
[Lagrangian formalism which describes the dynamics of non-relativistic extended
objects coupled to gravity](https://arxiv.org/pdf/hep-th/0409156.pdf)
* G. P. LEPAGE [HOW TO RENORMALIZE THE SCHRODINGER EQUATION](https://arxiv.org/pdf/nucl-th/9706029.pdf)
  > The transformation from the real theory to a simpler effective theory, valid for low-momentum processes, is achieved in two steps. First we introduce a momentum cutoff Λ that is of order the momentum at which new as yet unknown physics becomes important. Only momenta k < Λ are retained when calculating radiative corrections. a This means that our radiative corrections include only physics that we understand, and that there are no longer infinities. Of course we don’t really know the scale Λ at which new physics will be discovered, but, as we shall see, results are almost independent of Λ provided it is much larger than the momenta in the range being probed experimentally.
---
* Steven Weinberg
  * [EFT, Past and Future](https://pos.sissa.it/086/001/pdf) (Talk-> Article)  

[Lectures introduce some of the basic ideas of effective field theories.
The topics discussed include](https://arxiv.org/pdf/hep-ph/9606222v1.pdf)

> The basic premise of effective theories is that
dynamics at low energies (or large distances) does not depend on the details
of the dynamics at high energies (or short distances). As a result, low energy
physics can be described using an effective Lagrangian that contains only a few
degrees of freedom, ignoring additional degrees of freedom present at higher
energies.

> A non-renormalizable theory is just as good as a renormalizable
theory for computations, provided one is satisfied with a finite accu-
racy.


Einstein Gravity as a Symmetry Breaking Effect in Quantum Field Theoy

QCD Phenomenology based on a Chiral Effective Lagrangian

Only the title is enough to find the exact paper, since there is very limited
work available.

The confusion that arouse the EFT. Only talk about my own method.

[latex resource](http://artofproblemsolving.com/wiki/index.php?title=LaTeX:Math)

#### Strong gravitational effect in TeV scales
